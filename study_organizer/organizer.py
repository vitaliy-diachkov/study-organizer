import os
import shutil
import click


@click.group()
def main():
    pass


@main.command()
@click.argument('subject_name', required=True)
@click.argument('labs_count', required=True, type=int)
@click.option(
    '--destination', '--dest', '-D',
    help='Destination to folder should be created'
)
@click.option(
    '--template', '-T',
    help='A report template to be cloned into \"labs\" directories (.docx)'
)
def addsubject(subject_name, labs_count, destination, template):
    if not destination:
        destination = './'

    # get the executing .py file directory name
    dirname = os.path.dirname(__file__)

    if not template:
        template = os.path.join(dirname, 'assets/template.docx')

    subject_dir = os.path.join(destination, subject_name)
    methods_dir = os.path.join(subject_dir, 'methods')
    labs_dir = os.path.join(subject_dir, 'labs')
    labs_data = []
    for i in range(1, labs_count + 1):
        lab_dir = os.path.join(labs_dir, f'lab{i}')
        lab_report_path = os.path.join(lab_dir, f'{subject_name}_lab{i}.docx')

        labs_data.append({'dir': lab_dir, 'report': lab_report_path})

    try:
        os.makedirs(subject_dir)
        click.echo(f'Created {os.path.abspath(subject_dir)}')
        os.makedirs(methods_dir)
        click.echo(f'Created {os.path.abspath(methods_dir)}')
        os.makedirs(labs_dir)
        click.echo(f'Created {os.path.abspath(labs_dir)}')

        for lab_data in labs_data:
            os.makedirs(lab_data['dir'])
            click.echo('Created %s' % lab_data['dir'])
            report_path = shutil.copyfile(template, lab_data['report'])
            click.echo(
                f'Copied template from {os.path.abspath(template)} '
                f'to {os.path.abspath(report_path)}'
            )

    except OSError:
        click.echo(click.style(f'Unenable to create directory', fg='red'))
    else:
        click.echo(click.style(
            f'\nSuccessfully created subject template in {subject_dir}',
            fg='green'
        ))


if __name__ == '__main__':
    main()
